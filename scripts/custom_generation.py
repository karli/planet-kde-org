# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import importlib.resources as pkg_resources
import os

from hugoi18n import generation
from yaml import safe_load

domain_name = "planet-kde-org"


def generate():
    if not (os.path.isdir('content') and len(os.listdir('content')) > 0):
        return

    en_strings, configs, original_configs, _ = generation.read_sources()

    content_lang_codes = os.listdir('content')
    lang_config = list(configs['languages'].keys())
    with pkg_resources.open_text('hugoi18n.resources', 'languages.yaml') as f_langs:
        lang_names = safe_load(f_langs)
    for config_lang_code in lang_config:
        if config_lang_code not in content_lang_codes:
            del configs['languages'][config_lang_code]

    for hugo_lang_code in content_lang_codes:
        if hugo_lang_code in ['en']:
            continue
        else:
            lang_code = generation.revert_lang_code(hugo_lang_code)
            os.environ["LANGUAGE"] = lang_code
            _ = generation.gettext_func(domain_name)

            generation.generate_strings(en_strings, hugo_lang_code, _)
            generation.generate_languages(configs, lang_names, lang_code, hugo_lang_code)
            generation.generate_menu(configs, hugo_lang_code, _)
            generation.generate_description(configs, hugo_lang_code, _)
            generation.generate_title(configs, hugo_lang_code, _)

    generation.write_target(configs, original_configs)


if __name__ == "__main__":
    generate()
